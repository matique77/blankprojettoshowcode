import { ObjectId } from "bson";

class User {
    constructor(userId, email, infos){
        this.userId = userId; 
        this.email = email; 
        this.firstName = infos.firstName; 
        this.lastName = infos.lastName; 
    }
}

class Task {
  constructor({
    name,
    partition,
    status = Task.STATUS_OPEN,
    id = new ObjectId(),
  }) {
    this.ownerId = partition;
    this._id = id;
    this.name = name;
    this.status = status;
  }

  static STATUS_OPEN = "Open";
  static STATUS_IN_PROGRESS = "InProgress";
  static STATUS_COMPLETE = "Complete";

  static schema = {
    "name": "tasks",
    "properties": {
      "_id":  "objectId?",
      "assignedTo":  "objectId?",
      "description": "string?",
      
      "endDate":  "date?",
      "startDate":  "date?",
      "title": "string?",
      "name" : "string?", 
      "type":  "string?",
      "createBy":  "objectId?",
      "status":  "string?",
      "ownerId": "string"
    },
    "primaryKey": "_id"
  }
}


export {User, Task}; 