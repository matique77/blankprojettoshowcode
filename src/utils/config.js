
//Realm config 
class RealmConfig {
    constructor(schema, sync) {
        this.schema = schema; 
        this.sync.user = sync.user; 
        this.sync.partitionValue = sync.partitionValue; 
    }
}

const AppId = "fakeID";

export {AppId, RealmConfig}; 