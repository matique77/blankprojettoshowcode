import { combineReducers } from "redux";
import {ButtonMore, Header} from '../components/global/import'

; 

const reducers = combineReducers({
    buttonMoreReducer : ButtonMore.Reducer, 
    headerReducer :  Header.Reducer
})

export  {reducers}