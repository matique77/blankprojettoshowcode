import Realm from "realm";
import {AppId} from './config'

let app; 

export function getRealmApp() {
    if (app === undefined) {
       app = new Realm.App({id:AppId, timeout:1000});
    }
    return app;
  }
  