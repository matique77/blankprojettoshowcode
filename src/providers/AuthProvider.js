import React, { useContext, useState, useEffect, useRef }   from 'react';
import {AppId, RealmConfig} from '../utils/config'; 
import Realm from "realm";
import {getRealmApp} from "../utils/getRealmApp"
import User from '../utils/schemas'; 
import {
    ToastAndroid
  } from 'react-native';

// Access the Realm App.
const app = getRealmApp(); 

const AuthContext = React.createContext(null);

const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(app.currentUser);
    const realmRef = useRef(null);
  

    const createUserCustomData = async (user, customData) => {
      const mongo = user.mongoClient('fakeBd');
      const userData = mongo.db("fakBd").collection("users"); 
      await userData.insertOne(customData);
       await user.refreshCustomData(); 
    }


    const signIn = async function(email, password) {
    const credentials = Realm.Credentials.emailPassword(email, password);
      const newUser = await app.logIn(credentials).catch( (reason) => {
        throw reason; 
      });
      setUser(newUser)
      return user; 
  }

  const signOut = () => {
    if (user == null) {
      console.warn("Not logged in, can't log out!");
      return;
    }

    user.logOut();  
    setUser(null); 
  };


const  signUp =  async function(email, password, infos){    
    
  await app.emailPasswordAuth.registerUser(email, password).then( async value => {   
    
    let user = await signIn(email, password);
    await createUserCustomData(user, new User(user.id, email, infos)); 
  
    }).catch( (reason) => {
      throw reason; 
    }); 
    
}

return (
    <AuthContext.Provider
      value={{
          signIn, 
          signUp, 
          user
      }}
    >
      {children}
    </AuthContext.Provider>
  );

};

const useAuth = () => {
    const auth = useContext(AuthContext);
    if (auth == null) {
      throw new Error("useAuth() called outside of a AuthProvider?");
    }
    return auth;
};


export { AuthProvider, useAuth };