/*Authorization Screens*/
export {default as Login} from './login/login'
export {default as Register} from './register/register'

/*Tasks Screens*/
export { default as CreateTask} from './tasks/create'
export { default as IndexTask} from './tasks/index'
export { default as TasksNavigator} from './tasks/tasksNavigator'