import React, { useState }  from 'react';
import {enableScreens, ScreenContainer} from 'react-native-screens';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar, 
  TouchableOpacity, 
  ToastAndroid
} from 'react-native';

import {Screens} from "../../res/enums";
import {Strings} from "../../res/strings";
import Css from "../css/main"; 
import { TextInput } from 'react-native-gesture-handler';
import {useAuth} from '../../providers/AuthProvider'

export default function Register({navigation, route}){

  const [email, setEmail] = useState(''); 
  const [password, setPassword] = useState(''); 
  const [confirmPassword, setConfirmPassword] = useState(''); 
  const [infos, setInfos] = useState({ firstName : "", lastName : ""}); 
  const { user, signUp, signIn } = useAuth();

  const handleChange = (key, value) => {
    setInfos(prevState => ({
      ...prevState,
      [key] : value
    }))
  }; 

  return <View style={[Css.container]}>
    <Text style={Css.h1}>{Strings.register.title}</Text>
    
    <Text style={[Css.h3]}>{Strings.register.labels.infos}</Text>
    <View style={[Css.inline, Css.justifySpaceEvently]}>
      <View style={[Css.inputView, { width: '45%'}]}>
        <TextInput
          style={Css.inputText}
          placeholder={Strings.register.labels.lastName}
          onChangeText = { (lastName) => handleChange("lastName", lastName) }
          value = {infos.lastName}
        />
      </View>
      <View style={[Css.inputView, { width: '45%'}]}>
        <TextInput
          style={Css.inputText}
          placeholder={Strings.register.labels.firstName}
          onChangeText = { (firstName) => handleChange("firstName", firstName) }
          value = {infos.firstName}
        />
      </View>
    </View>


    <Text style={Css.h3}>{Strings.register.labels.email}</Text>
    <View style={Css.inputView}>
      <TextInput
        onChangeText={ email => setEmail(email) }
        style={Css.inputText}
        placeholder={Strings.register.labels.email}
      />
    </View>
 
    <Text style={Css.h3}>{Strings.register.labels.password}</Text>

    <View style={Css.inputView}>
      <TextInput
        secureTextEntry={true}
        onChangeText={ password => setPassword(password) }
        style={Css.inputText}
        placeholder={Strings.register.labels.password}
      />
    </View>

    <View style={Css.inputView}>
      <TextInput
        secureTextEntry={true}
        onChangeText={ confirmPassword => setConfirmPassword(confirmPassword) }
        style={Css.inputText}
        placeholder={Strings.register.labels.passwordConfirmation}
      />
    </View>

    <View style={[Css.inline, Css.justifySpaceEvently]}>

      <TouchableOpacity onPress={async () => {
            await signUp(email, password, infos).then((value) => {  
              navigation.navigate(Screens.tasks)
            }).catch(reason => {
              ToastAndroid.show(reason.message, ToastAndroid.SHORT);
            });  
          
        }} style={Css.button}>
            <Text style={Css.loginText}>{Strings.buttons.register}</Text>
      </TouchableOpacity>
      
      <TouchableOpacity onPress={() => { navigation.navigate(Screens.login)}} style={Css.button}>
            <Text style={Css.loginText}>{Strings.buttons.cancel}</Text>
      </TouchableOpacity>

    </View>

    </View>; 
}; 
