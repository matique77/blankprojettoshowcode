import React, { useState }  from 'react';
import {enableScreens, ScreenContainer} from 'react-native-screens';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar, 
  TouchableOpacity, 
  ToastAndroid
} from 'react-native';
import {Screens} from "../../res/enums"
import {Strings} from "../../res/strings";
import Css from "../css/main"; 
import { TextInput } from 'react-native-gesture-handler';
import {useAuth} from '../../providers/AuthProvider'

export default  function({navigation, route}){
  const [email, setEmail] = useState(''); 
  const [password, setPassword] = useState(''); 
  const { user, signUp, signIn } = useAuth();
  return ( <View style={ [Css.container, {justifyContent: 'center'}]}>
    <Text style={Css.h1}>{Strings.login.title}</Text>
    <View style={Css.inputView}>
      <TextInput
        onChangeText={email => setEmail(email)}
        style={Css.inputText}
        placeholder={Strings.login.labels.username}
      />
    </View>
    <View style={Css.inputView}>
      <TextInput
      secureTextEntry={true}
        onChangeText={password => setPassword(password)}
        style={Css.inputText}
        placeholder={Strings.login.labels.password}
      />
    </View>
    <TouchableOpacity onPress ={ async () => {
        await signIn("mathieu.morin@myaparts.com","123456").then((user) => {  
          navigation.navigate(Screens.tasks)
        } ).catch( reason => {
          ToastAndroid.show(reason.message, ToastAndroid.SHORT);
        })
      } } style={Css.button}>
          <Text style={Css.loginText}>{Strings.login.buttons.logIn}</Text>
    </TouchableOpacity>
    <View style={[Css.inline, Css.justifyCenter]}>
      <Text onPress={() => {
          navigation.navigate(Screens.register)
    }} style={Css.label}>{Strings.login.labels.notRegister}</Text>
    </View>
    
    </View>); 
}; 
