import React, { useState, useEffect }  from 'react';
import {
  View,
  Text
} from 'react-native';

import {Strings} from "../../res/strings";
import Css from "../css/main"; 
import {useAuth} from "../../providers/AuthProvider"; 
import {useTasks} from "../../providers/TasksProvider"; 

import {useDispatch} from 'react-redux'; 

import DatePicker from 'react-native-date-picker'

import { Input, Header, Button} from '../../components/global/import'; 

import { createStackNavigator } from '@react-navigation/stack';


const Stack = createStackNavigator();


const Page1 = (props) => {
  
  const { user } = useAuth();
  const { tasks,  createTask, deleteTask, setTaskStatus, } = useTasks();
  const [name, setName] = useState(''); 
  const dispatch = useDispatch()

  useEffect(() => {
    const unsubscribe = () =>{
          props.navigation.addListener('focus', (e) => {
            
            dispatch( {type: Header.ACTION.SET_TITLE, payload: {title :"Create a task"}})
          });
          props.navigation.addListener('blur', () => {
            
            dispatch( {type: Header.ACTION.SET_TITLE, payload: {title :""}})
          });
    }

    unsubscribe();
    
    return unsubscribe;
  }, [props.navigation])

 
  return (
  <View style={Css.container}>
    
      <View style={{justifyContent:'flex-start',width:'100%',height:'90%',  justifyContent:'space-evenly', marginTop:10, marginLeft: '10%' }}>
        
          <View>
            <Text style={Css.h3}>{Strings.tasks.create.title}</Text>
            <Input />
          </View>

          <View>
            <Text style={Css.h3}>Affecté à</Text>
            <Text style={[Css.h3, {opacity: 0.5}]}>Mathieu </Text>
          </View>
          
          <View>
            <Text style={Css.h3}>Responsable</Text>
            <Input />
          </View>

          <View>
          <Text style={Css.h3}>Endroit</Text>
          <Input />
          </View>
      
      </View>
      
      <View style={{flexDirection: 'row', marginTop:'auto',justifyContent:'space-around',width: '100%'}}>
          <Button 
            title="Page Suivante"
            onPress={() =>{
              props.navigation.navigate("Page 2"); 
            }}
          />
      </View>
        
    </View>)
}; 


const Page2 = (props) => {
  
  const dispatch = useDispatch()
  useEffect(() => {
    const unsubscribe = () =>{
          props.navigation.addListener('focus', (e) => {
        
            dispatch( {type: Header.ACTION.SET_TITLE, payload: {title :"Create a task"}})
          });
          props.navigation.addListener('blur', () => {
            
            dispatch( {type: Header.ACTION.SET_TITLE, payload: {title :""}})
          });
    }
    unsubscribe();
    
    return unsubscribe;
  }, [props.navigation]); 

  return (<View style={[Css.container, {width:'100%', height:'100%',  justifyContent:'space-evenly'}]}>
   
   <View>
    <Text style={Css.h3}>Date de fin</Text>
    <DatePicker
      date={new Date()}
      mode={'datetime'}
      style={{backgroundColor: 'white'}}
    />
    </View>

  <View style={{flexDirection: 'row', justifyContent:'space-around',width: '100%'}}>
    <Button title="Page précédente"
      onPress={()=>{props.navigation.navigate("Page 1")}}
    />

    <Button 
    title="Terminé"
    onPress={() =>{
      props.navigation.navigate("Page 2"); 
    }}
    />
  </View>
        
  </View>)
}

export default function Create(props){
  return(
  <Stack.Navigator screenOptions={{
    headerShown: false,
    cardStyle: { backgroundColor: 'transparent' },
    cardStyleInterpolator: ({ current: { progress } }) => ({
      cardStyle: {
        opacity: progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
        }),
      },
      overlayStyle: {
        opacity: progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 0.5],
          extrapolate: 'clamp',
        }),
      },
    }),
  }} headerMode={false}>
    <Stack.Screen name={"Page 1"} component={Page1} />
    <Stack.Screen name={"Page 2"} component={Page2} />
 </Stack.Navigator>); 
}