import React, {  useEffect}  from 'react';
import {
  ScrollView,
  View
} from 'react-native';
import {Screens} from "../../res/enums"
import {Strings} from "../../res/strings";
import Css from "../css/main"; 
import {useAuth, useTasks} from '../../providers/import'
import {ScrollDrawer, ButtonMore, Header} from "../../components/global/import"
import {Task} from "../../components/tasks/task"
import {useDispatch} from 'react-redux'; 


export default  function Index(props){
  const { user } = useAuth();
  const { tasks,  createTask, deleteTask, setTaskStatus, } = useTasks();
  const dispatch = useDispatch()
  

  useEffect(() => {
    const unsubscribe = () => {
      props.navigation.addListener('focus', () => {
        dispatch( {type: Header.ACTION.SET_TITLE, payload: {title :"Tasks List"}})
        dispatch( {type: ButtonMore.ACTION.TOGGLE_VISIBILITY})
        dispatch( {type: ButtonMore.ACTION.SET_ACTION, payload: { onPress :() => { props.navigation.navigate(Screens.create)} }  })
      });
      
      props.navigation.addListener('blur', () => {
        dispatch({type:  ButtonMore.ACTION.TOGGLE_VISIBILITY})
        dispatch( {type: Header.ACTION.SET_TITLE, payload: {title :""}})

      });
    } 
    
    unsubscribe(); 
    return unsubscribe;
  }, [props.navigation])

  const marginVertical = 5; 
  const taskHeight = 50; 

  return (
    <View style={ [ Css.container] }>
      <ScrollView style={{width:'100%'}} showsVerticalScrollIndicator={false}>
          
          <ScrollDrawer childrenHeight={((marginVertical*2)+taskHeight)} style={{width: '90%', marginLeft: 'auto', marginRight:'auto', marginTop:20}} title={Strings.tasks.index.drawersTitles.today}>
            <Task onDelete={() => {}} height={taskHeight} marginVertical={marginVertical}  title="Tache 1 "  time="10:01"/>
            <Task onDelete={() => {}} height={taskHeight} marginVertical={marginVertical}  title="Tache de la journée.."  time="10:02"/>
            <Task onDelete={() => {}} height={taskHeight} marginVertical={marginVertical}  title="Tache 1 "  time="10:01"/>
            <Task onDelete={() => {}} height={taskHeight} marginVertical={marginVertical}  title="Tache de la journée.."  time="10:02"/>

          </ScrollDrawer>
          
          <ScrollDrawer childrenHeight={((marginVertical*2)+taskHeight)} style={{width: '90%', marginLeft: 'auto', marginRight:'auto', marginTop:20}} title={Strings.tasks.index.drawersTitles.thisWeek}>
          </ScrollDrawer>
          
          <ScrollDrawer style={{width: '90%', marginLeft: 'auto', marginRight:'auto', marginTop:20}} title={Strings.tasks.index.drawersTitles.nextWeek}>
          </ScrollDrawer>
          
          <ScrollDrawer style={{width: '90%', marginLeft: 'auto', marginRight:'auto', marginTop:20}} title={Strings.tasks.index.drawersTitles.completed}>

          </ScrollDrawer>
        </ScrollView>
    </View>
  )
}; 
