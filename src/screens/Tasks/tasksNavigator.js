/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createBottomTabNavigator, createStackNavigator } from '@react-navigation/stack';

import {IndexTask, CreateTask} from '../import';


const Stack = createStackNavigator();

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar
  } from 'react-native';


import { TasksProvider } from "../../providers/TasksProvider";
import { Screens } from '../../res/enums';


  export default function TasksNavigator({navigation, route}) {
    return (
      <TasksProvider>
        <Stack.Navigator headerMode={false}>
         <Stack.Screen name={Screens.index} component={IndexTask} />
         <Stack.Screen name={Screens.create} component={CreateTask} />
        </Stack.Navigator>
      </TasksProvider>
    )
  }