import { StyleSheet } from "react-native"

export default StyleSheet.create({

  inline :{
    display: "flex", 
    flexDirection: "row", 
    width: '100%'
  }, 

  justifySpaceBetween :{
    justifyContent: "space-between"
  }, 

  justifySpaceEvently :{
    justifyContent: "space-evenly"
  }, 

  justifyCenter :{
    justifyContent: "center"
  }, 

  label :{
    color: 'white', 
    marginRight: 10
  }, 

  h1: {
    fontWeight:"bold",
    fontSize: 50,
    color: '#fa5a5a', 
    marginVertical: 30
  },

  h2: {
    fontWeight:"bold",
    fontSize: 30,
    color: '#fa5a5a', 
    marginTop: 20
  },

  h3:{ textAlign: "left",  
        width: '100%', 
        marginBottom: 10, 
        fontSize: 22, 
        fontWeight: 'bold', 
        color: 'white',
        marginLeft:'5%'
      }, 
      
  button:{
    width : '40%',
    backgroundColor:"#fb5b5a",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:10,
    marginBottom:10
  },

  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center'
  },

  inputView:{
    width:"80%",
    backgroundColor:"#465881",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  }, 
  inputText:{
    height:50,
    color:"white"
  },
    engine: {
      position: 'absolute',
      right: 0,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',

    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',

    },
    highlight: {
      fontWeight: '700',
    },
    footer: {

      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    }
  });