export const Strings = {
    "login": {
        "title": "Application",
        "labels": {
            "username": "Username",
            "password": "Password", 
            "notRegister" : "Not Register?", 
            "help" : "Help"
        },
        "buttons": {
            "logIn": "LOGIN"
        }
    },

    "welcome" : {
        "title" : "Welcome!",
        "buttons" :{
            "signOut" : "Sign Out"
        }
    },

    "register": {
        "title": "Register",
        "labels": {
            "infos" : "Infos",
            "firstName" : "First Name",
            "lastName" : "Last Name",
            "email" : "Email",
            "password" : "Password", 
            "passwordConfirmation" : "Confirm Password"
            
        }
    },
        
    "tasks" : {
        "title" : "Tasks",
        "index" : {
                "drawersTitles" : {
                "today" : "Today", 
                "thisWeek" : "This Week",
                "nextWeek" : "Next Week",
                "completed" : "Completed"
             }
            }
        ,
        "create" : {
                "title" : "Title", 
                "assignedTo" : "Assigned to",
                "responsible": "Responsible", 
                "owner" : "Owner", 
                "where" : "Where"
            }
           
        },
   

        "buttons":{
            "cancel" : "Cancel",
            "register" : "Register"
        }
}