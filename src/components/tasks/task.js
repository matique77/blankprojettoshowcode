import React, { useRef, useEffect, useState } from 'react';
import {
    View,
    Text,
    Animated, 
    Dimensions,
    PanResponder, 
    LogBox
    
  } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export const Task = (props) => {
  useEffect(() => {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])

  const windowWidth = Dimensions.get('window').width;
  let elem = useRef(null);  
  let lock = false; 
  let value = useRef(0);  
  console.log(elem.nativeEvent)
  const [widthDelete, setWidthDelete] = useState(windowWidth/4); 

  const [deleteButtonOpacity, setDeleteButtonOpacity] = useState(1);
  

  const pan =  useRef(new Animated.ValueXY({x:0, y:0})).current;
  
  const panResponder = useRef(PanResponder.create({
    onMoveShouldSetPanResponder: () => true,
    onPanResponderGrant: () =>{
      if(lock){
        pan.setOffset({x:value.current *-1, y:0})
      } else{
        pan.setOffset({x:0, y:0})
      }
      
    },

    onPanResponderMove: Animated.event([
        null, 
        {dx:pan.x, dy:pan.y}
      ], {}),
          
    onPanResponderRelease: (evnt, gestureState) => {

      let dx = 0; 
      
      if(gestureState.dx * -1 > value.current && !lock ){
          dx = value.current * -1; 
          lock = true; 
      } else if( lock && gestureState.dx > 0 ){
        dx = value.current; 
        lock =false; 
      }

      Animated.spring(pan, { toValue: {x:dx, y:0}}).start(); 
      
    }
  })
  ).current;

    return (
     <View style={{ flexDirection:'row', alignItems:'center'}}>
        <Animated.View
        style={{  
          width:'100%',
          backgroundColor: '#FFFFFF',
          height: props.height, 
          flexDirection:'row', 
          alignItems:'center', 
          marginVertical: props.marginVertical, 
          borderRadius: 5,
          transform: [{translateX: pan.x}]
        }}
        {...panResponder.panHandlers}
        >
            <Text onPress={() => {  }}  style={{borderColor: '#bf1515', borderWidth: 3, marginLeft:20,  width: 30, height: 30, borderRadius: 50}}></Text>
            <Text style={{marginLeft: 30, fontSize: 20, fontWeight: 'bold',  color: '#000000'}}>{props.title}</Text>
            <Text style={{marginLeft:'auto', marginRight:'5%'}}>{props.time}</Text>
        </Animated.View>
        <View
        style={{backgroundColor:'red', 
                      height:'80%', 
                      zIndex:-5, 
                      position:'absolute', 
                      right:0, 
                      width:widthDelete, 
                      justifyContent:'center', 
                      borderRadius: 5,
                      opacity: deleteButtonOpacity
                      }}
                      ref={elem} 
                      onLayout={event => {
                        value.current = event.nativeEvent.layout.width; 
                      }}
                      onTouchStart ={()=>{
                        setDeleteButtonOpacity(0.8); 
                      }}
                      onTouchEnd= {()=>{
                        setDeleteButtonOpacity(1); 
                        props.onDelete(); 
                      }}
                      >

        <Text 
          style={{
            textAlign:'center',
            textAlignVertical:'bottom', 
            padding: 5,
            fontWeight:'bold'
       
          }}       
          >SUPPRIMER</Text>
        </View>
       
        </View>
    
    )
}