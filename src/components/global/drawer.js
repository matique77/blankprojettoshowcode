import React, { useState, useRef}  from 'react';
import {
  ScrollView,
  View,
  Text,
  Animated, 
  Dimensions
} from 'react-native';


export const ScrollDrawer  = (props) => {
  const [opacity, setOpacity] = useState(1); 
  const [status, setStatus] = useState(false); 

  const windowHeight = Dimensions.get('window').height;
  let drawerHeight = 0; 
  
  if('children' in props && 'childrenHeight' in props){
    let nbChildren = React.Children.count(props.children); 
    drawerHeight =Math.min(windowHeight, (nbChildren*props.childrenHeight))
  }

  const drawerAnimValue = useRef(new Animated.Value(0)).current; 
  const openDrawer = Animated.timing(    
    drawerAnimValue, 
    {
      useNativeDriver: false,
      toValue: drawerHeight, 
      duration: 200
    }); 
  
    const closeDrawer = Animated.timing(    
      drawerAnimValue, 
      {
      useNativeDriver: false,
        toValue: 0, 
        duration: 200
      }); 


  const toggleDrawer = () => {
    
    if(status){
      openDrawer.stop(); 
      closeDrawer.start(); 
    } else{
      closeDrawer.stop(); 
      openDrawer.start(); 
    }

    setStatus(!status); 
  }

  
  return (
    <View style={props.style}>
    <View
      style={{
        marginLeft:'1%',
        padding: 5
      }}
      onTouchStart={() =>{
        setOpacity(0.6);
        
      }}
      
      onTouchEnd={() =>{
        setOpacity(1);
        toggleDrawer()
      }}
    >
      <Text style={{color: 'white', opacity: opacity,  alignSelf: 'flex-start', fontSize: 20, fontWeight: 'bold'}}>{props.title}</Text>
    </View>
      
      <Animated.View style={{ height: drawerAnimValue}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {props.children}
        </ScrollView>
      </Animated.View>
      
    </View>
  )
}; 
