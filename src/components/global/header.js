import React, { useState, useEffect, useRef}  from 'react';
import {
  View, Text
} from 'react-native';
import {useSelector} from 'react-redux'
import Css from '../../screens/css/main'; 

 const ACTION = {
    SET_TITLE : "SET_TITLE", 
    TOGGLE_USER_BUBBLE : "TOGGLE_USER_BUBBLE"
}

 const Reducer = (state = {title : null, visibility: false}, action) => {
    
    const {type, payload} = action; 
    
    switch (type) {
        case ACTION.SET_TITLE:
            return {
                ...state,
                title : payload.title
            }
            break;
        case (ACTION.TOGGLE_USER_BUBBLE):
            return {
                ...state,
                visibility: !state.visibility
            }
        default:
            return state; 
            break;
    }
}

 const Interface = (props) => {
    const headerSelector = useSelector((state) => state.headerReducer); 


    return (<View style={{flexDirection:'row', backgroundColor: '#003f5c', height:' 8%', paddingRight:'5%', paddingBottom:2, alignItems: 'flex-end', justifyContent:'center' }}>
                <Text style={[{marginLeft:'auto'}, Css.h2]}>{headerSelector.title}</Text>
                <View style={{display:(headerSelector.visibility ? 'flex' : 'none' ) ,backgroundColor: 'white', height:35, width:35, borderRadius: 50, marginLeft:'auto'}}></View>
            </View>)
}

const Header = {
    ACTION, 
    Interface, 
    Reducer
}

export {Header}