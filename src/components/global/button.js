import React from 'react';
import {
  Text,
  TouchableOpacity
} from 'react-native';

import Css from '../../screens/css/main'; 


export const Button = (props) => {

    let onPress = () =>{}; 
    if("onPress" in props){
        onPress = props.onPress; 
    }

    return (<TouchableOpacity style={Css.button}
                onPressOut={onPress}
            >
                 <Text style={Css.loginText}>{props.title}</Text>
            </TouchableOpacity>)
}