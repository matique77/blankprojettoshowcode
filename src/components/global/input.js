import Css from '../../screens/css/main';
import React from 'react';
import {
  View, 
  TextInput
} from 'react-native';



export const Input = (props) => {

    let onChangeText = () => {}; 

    if(props.onChangeText){
        onChangeText = props.onChangeText; 
    }

    return (<View style={Css.inputView}>
        <TextInput
          style={Css.inputText}
          placeholder={props.placeholder}
          onChangeText = {onChangeText}
          value = {props.value}
        />
    </View>)
}