import { createSelector } from 'reselect';
import React, { useState }  from 'react';
import {
  View,
  Text,
} from 'react-native';


import {useDispatch, useStore, useSelector} from 'react-redux'; 


const ACTION = {
  "TOGGLE_VISIBILITY" : "TOGGLE_VISIBILITY",
  'SET_ACTION' : 'SET_ACTION', 
  'CALL_ACTION' : 'CALL_ACTION'
}

const Reducer = (state = { visibility : false, onPress : null }, action) => {
  
  const {type, payload} = action; 
  
  switch(type){
      case ACTION.TOGGLE_VISIBILITY :
          return {
              ...state, 
              visibility : !state.visibility
          }
      case ACTION.SET_ACTION : 
          return {
              ...state,
              onPress: payload.onPress
          } 
      default: 
          return state; 
  }
}

 const Interface = (props) =>{
  
    const [opacity, setOpacity] = useState(1); 
    const visibility = useSelector((state) => state.buttonMoreReducer.visibility); 
    const onPress = useSelector((state) => state.buttonMoreReducer.onPress); 

    return ( visibility &&
      <View
          onTouchStart={() =>{
            setOpacity(0.8);
          }}

          onTouchEnd={() =>{
            setOpacity(1);
            onPress(); 
          }}
      >
        <Text style={{
        fontSize: 35, 
        textAlign: 'center', 
        backgroundColor:'#bf1515', 
        color:'#ffffff', 
        width:50,
        height:50, 
        alignSelf:'center', 
        borderRadius:100, 
        position:'absolute',
        bottom:25, 
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        opacity:opacity}}
      >+</Text>
      </View>)
}

const ButtonMore ={
  Interface, Reducer, ACTION 
}

export {ButtonMore}