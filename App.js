//React import
import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//Screens
import { Screens } from './src/res/enums';
import { Login, Register, TasksNavigator} from './src/screens/import';

//Store
import {Provider} from 'react-redux';
import store from './src/utils/configureStore';

//Provider
import { AuthProvider } from "./src/providers/import";

//Components
import { ButtonMore, Header } from './src/components/global/import'; 
 
const Tab = createBottomTabNavigator();

const App = () => 
<Provider store={store}>
<AuthProvider>
<Header.Interface />
<NavigationContainer> 
  <Tab.Navigator
      screenOptions={{
        headerShown: false
      }}
  >
    <Tab.Screen
      name= {Screens.login}
      component={Login}
      />
    
    <Tab.Screen
      name= {Screens.register}
      component={Register}
    />

      <Tab.Screen
      name= {Screens.tasks}
      component={TasksNavigator}
    />
  </Tab.Navigator>
      <ButtonMore.Interface/>
</NavigationContainer>
</AuthProvider>
</Provider>

export default App;
